import { LitElement, html, css } from "../../node_modules/lit-element/lit-element.js";

class EditUser extends LitElement {
  static get properties() {
    return {
      user: { type: Object }
    };
  }

  window.uid = -1;
  function editUser(uid) {
    fetch(`api/fetchUser.php?id=${uid}`) //Fetches the user for the ability to edit
    .then(res=>res.json())
    .then(user=>{
      window.uid = user.uid;
      document.getElementById('uname').value = user.uname;
      document.getElementById('firstName').value = user.firstName;
      document.getElementById('lastName').value = user.lastName;
    })
  }

  document.querySelector('input[type="submit"]').addEventListener('click', e=>{
    e.preventDefault();
    if (window.uid>-1) {
      const data = new FormData(e.target.form);
      data.append('uid', window.uid);
      fetch('api/updateUser.php', {
        method: 'POST',
        body: data
      }).then(res=>res.json())
      .then(data=>{
        const response = document.querySelector('.response'); //When clicked, if successful added success message
        response.style.display = 'block';
        if (data.status=='success') {
          response.style.color = "#7AD752";
          response.innerHTML = "Information about the user is updated";
          fetchUsers();
        } else {
          response.style.color = "#ED3E2F";
          response.innerHTML = "Could not update information of the user"; //If unsuccessful show this message
          console.log(data.msg);
        }
      })
    }
  }
customElements.define('edit-user', EditUser);
