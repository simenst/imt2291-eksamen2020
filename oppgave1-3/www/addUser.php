<?php
require_once 'vendor/autoload.php';

$loader = new Twig_Loader_Filesystem('./oppgave1');
$twig = new Twig_Environment($loader, array(
    /*'cache' => './compilation_cache',*/ /* Only enable cache when everything works correctly */
));

if (!isset($_POST['uname'])) {  // Show form to create user
  echo $twig->render('addUser.html', array());
} else {                        // User data exists, create new user
  require_once 'classes/DB.php';
  $db = DB::getDBConnection();
  $err['code'] = 1;
  $err['clue'] = 'One of the fields is not correctly filled out';
  // Check to see if all fields are filled in
  if (isset($_POST['uname'])&&isset($_POST['pwd'])&&isset($_POST['firstName'])&&isset($_POST['lastName'])) {
    $err['code'] = 0;
    if (!preg_match("/[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$/",$_POST['uname'])) {  // Checks password
      $err['code'] = 1;
      $err['clue'] = 'Invalid e-mail';
    } else if (strlen($_POST['pwd'])<8) {
      $err['code'] = 1;
      $err['clue'] = 'Password too short. Must contain at least 8 characters';
    } else if (strlen($_POST['firstName'])<2) {
      $err['code'] = 1;
      $err['clue'] = 'First name is to short';
    } else if (strlen($_POST['lastName'])<3) {
      $err['code'] = 1;
      $err['clue'] = 'Last name is too short';
    }
  }
  if ($err['code']==1) {  // One or more fields are wrong
    echo $twig->render('addUser.html', $err);
  } else {                // All fields are OK
    $stmt = $db->prepare('INSERT INTO user (uname, pwd, firstName, lastName) VALUES(?, ?, ?, ?)');
    $stmt->execute(array($_POST['uname'], password_hash($_POST['pwd'], PASSWORD_DEFAULT), $_POST['firstName'], $_POST['lastName']));
    if ($stmt->rowCount()==1) { // User added to DB
      echo $twig->render('userAdded.html', $_POST);
    } else {                    // User not added to DB
      $err['code'] = 1;
      $err['clue'] = 'Could not add user to the database.';
      echo $twig->render('addUser.html', $err);
    }
  }
}
